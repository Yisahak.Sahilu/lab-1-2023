package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int length1 = word1.length();
        int length2 = word2.length();
        int length3 = word3.length();

        String longest = word1;
        
        if (length2 > length1){
            longest = word2;
        }
        else if (length2 == longest.length()){
            longest = longest + "\n" + word2;
        }
        if (length3 > longest.length()){
            longest = word3;
        }
        else if (length3 == length2){
            longest = longest + "\n" + word3;
        }

        System.out.println(longest);


    }


    public static boolean isLeapYear(int year) {
        if (year % 4 == 0){
            if (year % 100 == 0){
                if (year % 400 == 0){
                    return true;
                }
                return false;
            }
            return true;
        }
        else{
            return false;
        }
    }

    public static boolean isEvenPositiveInt(int num) {

        if (num % 2 == 0 && num > 0){
            return true;
        }
        else{
            return false;
        }
    }

}
