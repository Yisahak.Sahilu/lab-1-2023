package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        for(int i = 0; i < grid.size(); i++){
            if (i == row){
                grid.remove(i);
            }
        }
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        
        int rowSum;
        int colSum;
        ArrayList<Integer> rowList = new ArrayList<>();
        ArrayList<Integer> colList = new ArrayList<>();

        for (ArrayList<Integer> i : grid){
            rowSum = 0;
            for (int j : i){
                rowSum += j;
            }
            rowList.add(rowSum);
        }
        
        for (int i = 0; i < grid.size(); i++){
            colSum = 0;
            for (int j = 0; j < grid.get(i).size(); j++){
                colSum += grid.get(j).get(i);
            }

            colList.add(colSum);
        }
        
        int prevRow = rowList.get(0);
        boolean equalRows = false;
        for (int i : rowList){
            equalRows = prevRow == i;
            
        }

        int prevCol = colList.get(0);
        boolean equalCols = false;
        for (int i : colList){
            equalCols = prevCol == i;


        }

        if(equalCols && equalRows){
            return true;
        }

        return false;
    }

}