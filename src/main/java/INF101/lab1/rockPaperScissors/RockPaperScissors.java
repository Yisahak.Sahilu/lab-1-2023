package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class RockPaperScissors {
	
    public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
        
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    String winner;
    String computerChoice;

    Random rd = new Random();

    public void run() {
        while (true){
            System.out.println("Let's play round " + roundCounter);

            String userChoice = readInput("Your choice (Rock/Paper/Scissors)?");

            while(!userChoice.equals("rock") && !userChoice.equals("paper") && !userChoice.equals("scissors")){
                System.out.println("I do not understand " + userChoice + ". " + "Could you try again?");
                userChoice = readInput("Your choice (Rock/Paper/Scissors)?");
            }


            computerChoice = rpsChoices.get(rd.nextInt(3));

            

            if(userChoice.equals(rpsChoices.get(0))){
                if(computerChoice.equals(rpsChoices.get(1))){
                    winner = "Computer wins!";
                    computerScore++;
                }
                else if (computerChoice.equals(rpsChoices.get(2))){
                    winner = "Human wins!";
                    humanScore++;
                }
            }

            if(userChoice.equals(rpsChoices.get(1))){
                if(computerChoice.equals(rpsChoices.get(2))){
                    winner = "Computer wins!";
                    computerScore++;
                }
                else if (computerChoice.equals(rpsChoices.get(1))){
                    winner = "Human wins!";
                    humanScore++;
                }
            }

            if(userChoice.equals(rpsChoices.get(2))){
                if(computerChoice.equals(rpsChoices.get(0))){
                    winner = "Computer wins!";
                    computerScore++;
                }
                else if (computerChoice.equals(rpsChoices.get(1))){
                    winner = "Human wins!";
                    humanScore++;
                }
            }
            
            if(userChoice.equals(computerChoice)){
                winner = "It's a tie!";
                
            }

            System.out.println("Human chose " + userChoice + ", computer chose " + computerChoice + "." + winner);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            String yn = readInput("Do you wish to continue playing? (y/n)?");

            if(yn.equals("y")){
                continue;
            }
            else{
                System.out.println("Bye bye :)");
                break;
            }

        }


    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}